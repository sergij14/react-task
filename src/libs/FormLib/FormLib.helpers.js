export const checkFormDirty = (initialValues, currentValues) => {
  try {
    return JSON.stringify(currentValues) === JSON.stringify(initialValues);
  } catch (e) {
    return true;
  }
};
